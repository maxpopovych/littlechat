package com.makki.kotlinstudies.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.makki.kotlinstudies.App
import com.makki.kotlinstudies.R
import com.makki.kotlinstudies.events.SocketResponse
import com.makki.kotlinstudies.managers.socket.SocketManager
import com.makki.kotlinstudies.model.MessageAsset

/**
 * @author Max.Popovych on 02.06.18.
 */
@SuppressLint("RtlHardcoded")
class MessageLayoutWrapper {

    var contex: Context? = null
    var layout: ViewGroup? = null
    var currentUser: String? = null

    public fun setTarget(layout: ViewGroup?): MessageLayoutWrapper {
        if (layout == null) {
            throw IllegalArgumentException("Target is null")
        }

        this.layout = layout
        this.contex = layout.context
        return this
    }

    public fun feed(msg: MessageAsset) {
        if (contex == null || layout == null) {
            throw IllegalArgumentException("Context or layout == null")
        }

        val ownMessage = msg.sender == getUser()

        val params = resolveParams(ownMessage)
        val view = MessageView(contex!!, msg.text)
        layout?.addView(view, params)
    }

    public fun feed(response: SocketResponse) {
        if (response.command == "text_message") {
            feed(MessageAsset(response))
        }

        val params = resolveParams(false)
        val view = TextView(contex).also {
            it.text = response.message
            it.setTextColor(App.applicationContext().resources.getColor(R.color.colorText)) }

        layout?.addView(view, params)
    }

    private fun resolveParams(ownMessage: Boolean): LinearLayout.LayoutParams {
        val params = LinearLayout.LayoutParams(ViewGroup.MarginLayoutParams.WRAP_CONTENT,
                ViewGroup.MarginLayoutParams.WRAP_CONTENT)
                .also {
                    it.setMargins(0, 4, 0, 4)
                    it.gravity = Gravity.LEFT }
        if (ownMessage) {
            params.gravity = Gravity.RIGHT
        } else {
            params.gravity = Gravity.LEFT
        }
        return params
    }

    private fun getUser(): String {
        if (currentUser == null) {
            currentUser = SocketManager.getLogin()
        }
        return currentUser!!
    }

}