package com.makki.kotlinstudies.views.rooms

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.makki.kotlinstudies.R
import com.makki.kotlinstudies.model.RoomAsset

/**
 * @author Max.Popovych on 01.06.18.
 */
class RoomsAdapter(private val list: ArrayList<RoomAsset>) : RecyclerView.Adapter<RoomsViewHolder>() {

    interface RoomClickCallback {
        public fun onRoomClick(asset: RoomAsset)
    }

    private var callback: RoomClickCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomsViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_room_element, parent, false)
        return RoomsViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: RoomsViewHolder?, position: Int) {
        holder?.onBindViewHolder(list[position], callback)
    }

    public fun setItems(list: ArrayList<RoomAsset>) {
        this.list.clear()
        this.list.addAll(list)
    }

    public fun setClickListener(callback: RoomClickCallback) {
        this.callback = callback
    }

}

class RoomsViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    @SuppressLint("SetTextI18n")
    fun onBindViewHolder(roomAsset: RoomAsset, callback: RoomsAdapter.RoomClickCallback?) {

        view.setOnClickListener { callback?.onRoomClick(roomAsset) }

        view.findViewById<TextView>(R.id.room_title_view).text = roomAsset.name
        view.findViewById<TextView>(R.id.room_descr_view).text = roomAsset.description
        view.findViewById<TextView>(R.id.room_count_view).text = "${roomAsset.usersCount}/${roomAsset.maxAllowed}"
    }

}