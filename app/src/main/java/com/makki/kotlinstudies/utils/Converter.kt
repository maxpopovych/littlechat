package com.makki.kotlinstudies.utils

import com.makki.kotlinstudies.events.SocketResponse
import org.json.JSONObject

/**
 * @author Max.Popovych on 31.05.18.
 */

class Converters {
    public val default = DefaultConverter()

    public val error = ErrorResponseConverter()

    val list = ArrayList<Converter>().also {
        it.add(JoinNoticeConverter())
        it.add(RoomResponseConverter())
        it.add(RoomJoinConverter())
        it.add(TextMessageConverter())
    }

    public fun forEach(obj: JSONObject): SocketResponse {
        for (interceptor in list) {
            if (interceptor.check(obj)) {
                return interceptor.generateResponse(obj)
            }
        }
        return default.generateResponse(obj)
    }
}

abstract class Converter {
    public fun check(obj: JSONObject): Boolean {
        return checkImpl(obj)
    }

    protected abstract fun checkImpl(obj: JSONObject): Boolean

    abstract fun generateResponse(obj: JSONObject): SocketResponse
}

class DefaultConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        return true
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(obj.optString("message", "N/A"), obj.optInt("id", -1))
                .also {
                    it.command = "unknown"
                    it.content = obj.optString("content", "{}")
                }
    }

}

class JoinNoticeConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        if (obj.get("command") == "join_notice") {
            return true
        }
        return false
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(obj.optString("message"), obj.optInt("id", -1))
                .also {
                    it.command = "join_notice"
                    it.content = obj.optString("content", "{}")
                }
    }
}

class RoomResponseConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        if (obj.get("command") == "room_list") {
            return true
        }
        return false
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(obj.optString("message"), obj.optInt("id", -1))
                .also {
                    it.command = "room_list"
                    it.content = obj.optString("content", "{}")
                }
    }
}

class RoomJoinConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        if (obj.get("command") == "room_join") {
            return true
        }
        return false
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(obj.optString("message"), obj.optInt("id", -1))
                .also {
                    it.command = "room_join"
                    it.content = obj.optString("content", "{}")
                }
    }
}

class ErrorResponseConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        return true
    }

    override fun generateResponse(obj: JSONObject) = SocketResponse.error()

    fun generateResponse(e: Throwable) = SocketResponse.error().also {
        it.message = e.message ?: it.message
    }
}

class TextMessageConverter : Converter() {
    override fun checkImpl(obj: JSONObject): Boolean {
        if (obj.get("command") == "text_message") {
            return true
        }
        return false
    }

    override fun generateResponse(obj: JSONObject): SocketResponse {
        return SocketResponse(obj.optString("message"), obj.optInt("id", -1))
                .also {
                    it.command = "text_message"
                    it.content = obj.optString("content", "{}")
                }
    }
}
