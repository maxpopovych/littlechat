package com.makki.kotlinstudies.model

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Max.Popovych on 01.06.18.
 */
class RoomAsset(val id: Int, val name: String, val description: String) : Parcelable{
    var usersCount = 0
    var maxAllowed = 0
    var users: List<UserAsset> = emptyList()

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString()) {
        usersCount = parcel.readInt()
        maxAllowed = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeInt(usersCount)
        parcel.writeInt(maxAllowed)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoomAsset> {
        override fun createFromParcel(parcel: Parcel): RoomAsset {
            return RoomAsset(parcel)
        }

        override fun newArray(size: Int): Array<RoomAsset?> {
            return arrayOfNulls(size)
        }
    }
}