package com.makki.kotlinstudies.managers

import android.util.Log
import com.makki.kotlinstudies.events.*
import com.makki.kotlinstudies.managers.socket.SocketManager
import com.makki.kotlinstudies.model.MessageAsset
import com.makki.kotlinstudies.model.RoomAsset
import io.reactivex.Maybe
import io.reactivex.Observable
import org.json.JSONObject

/**
 * @author Max.Popovych on 27.05.18.
 */
object ChatManager {

    private val socket = SocketManager

    public fun trackMessagesFrom(senderId: Int): Observable<MessageAsset> {
        return socket.trackResponses()
                .filter {
                    it.type == SResponseType.StringMessage || it.type == SResponseType.ByteMessage
                }.filter {
                    it.command == "text_message" //TODO: change to enum
                }.map { MessageAsset(it) }
    }

    public fun trackBroadcasts(): Observable<SocketResponse> {
        return socket.trackResponses()
                .filter {
                    it.type != SResponseType.OpenMessage
                }.filter {
                    it.command != "text_message" //TODO: change to enum
                }
    }

    public fun register(login: String): Maybe<SocketResponse> {
        socket.setLogin(login)

        return socket.sendPendingRequest(RegisterRequest(login))
    }

    public fun getRoomList() = socket
            .sendPendingRequest(RoomListRequest())
            .map {
                val array = JSONObject(it.content).getJSONArray("rooms")
                val list = ArrayList<RoomAsset>()
                for (i in 0 until array.length()) {
                    val obj = JSONObject(array.getString(i))
                    Log.e("ChatManager", "Json -> $obj")
                    val id = obj.optInt("id", i)
                    val name = obj.optString("name", "N/A")
                    val description = obj.optString("description", "No description provided")
                    val usersCount = obj.optInt("usersCount", -1)
                    val maxAllowed = obj.optInt("maxAllowed", 99)
                    list.add(RoomAsset(id, name, description).also {
                        it.usersCount = usersCount
                        it.maxAllowed = maxAllowed
                    })
                }
                return@map list
            }

    public fun joinRoom(roomName: String) = socket
            .sendPendingRequest(RoomJoinRequest(roomName))

    public fun sendTextMessage(text: String, roomName: String) = socket
            .sendRequest(MessageRequest(text, roomName))
}