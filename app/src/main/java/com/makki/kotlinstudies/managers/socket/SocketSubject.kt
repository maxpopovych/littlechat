package com.makki.kotlinstudies.managers.socket

import android.util.Log
import com.makki.kotlinstudies.events.*
import com.makki.kotlinstudies.utils.Converters
import io.reactivex.subjects.Subject
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString
import org.json.JSONObject
import java.nio.charset.Charset

/**
 * @author Max.Popovych on 13.05.18.
 */

class SocketSubject(private val subject: Subject<SocketResponse>) : WebSocketListener() {

    companion object {
        private const val TAG: String = "SocketSubject"
    }

    private val converters = Converters()

    override fun onOpen(webSocket: WebSocket, response: Response) {
        val event = SocketResponse(response.message(), -1)
                .also {
                    it.type = SResponseType.OpenMessage
                }
        subject.onNext(event)
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        val obj = JSONObject(text)

        subject.onNext(converters.forEach(obj).also {
            it.type = SResponseType.StringMessage
        })
    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        val obj = JSONObject(bytes.string(Charset.defaultCharset()))

        subject.onNext(converters.forEach(obj).also {
            it.type = SResponseType.ByteMessage
        })
    }

    override fun onClosing(webSocket: WebSocket?, code: Int, reason: String) {
        val event = SocketResponse(reason, -1)
                .also {
                    it.type = SResponseType.ClosingMessage
                }
        subject.onNext(event)
    }

    override fun onClosed(webSocket: WebSocket?, code: Int, reason: String) {
        val event = SocketResponse(reason, -1)
                .also {
                    it.type = SResponseType.ClosedMessage
                }
        subject.onNext(event)
    }

    override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
        if (t != null) {
            Log.e(TAG, "Encountered error in socketManager", t)
            subject.onNext(converters.error.generateResponse(t))
        }
    }
}
