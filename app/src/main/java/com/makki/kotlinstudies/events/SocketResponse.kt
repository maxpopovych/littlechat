package com.makki.kotlinstudies.events

/**
 * @author Max.Popovych on 14.05.18.
 */
class SocketResponse(
        var message: String,
        var id: Int) {
    var content = ""
    var command = "Unknown"

    var pending: Boolean = false // used to skip the observer

    var type: SResponseType = SResponseType.UnknownMessage

    companion object {
        fun error() = SocketResponse("error", -1).also {
            it.type = SResponseType.FailureMessage
        }
    }
}

enum class SResponseType {
    OpenMessage,
    StringMessage,
    ByteMessage,
    ClosingMessage,
    ClosedMessage,
    FailureMessage,
    UnknownMessage
}