package com.makki.kotlinstudies.events

import com.makki.kotlinstudies.managers.socket.SocketManager
import org.json.JSONObject

/**
 * @author Max.Popovych on 27.05.18.
 */
abstract class SocketRequest {
    var id: Int = SocketManager.getNextRequestId()
    val timestamp: Long = System.currentTimeMillis() / 1000
    open var type: String = "general"

    final override fun toString(): String = this.toJson().toString()

    open fun toJson(): JSONObject {
        return JSONObject().put("id", id)
                .put("timestamp", timestamp)
                .put("type", type)
    }
}

class MessageRequest(val message: String, var roomName: String) : SocketRequest() {
    override var type = "text_message"

    override fun toJson() = super.toJson()
            .put("room_name", roomName)
            .put("message", message)!!
}

class RegisterRequest(private val login: String) : SocketRequest() {
    override var type = "register"

    override fun toJson() = super.toJson()
            .put("login", login)!!
}

class RoomListRequest : SocketRequest() {
    override var type = "room_list"
}

class RoomJoinRequest(private val roomName: String) : SocketRequest() {
    override var type = "room_join"

    override fun toJson() = super.toJson()
                .put("room_name", roomName)!!
}