package com.makki.kotlinstudies

import android.app.Application
import android.content.Context
import android.os.Handler

/**
 * @author Max.Popovych on 29.05.18.
 */
class App : Application() {
    init {
        instance = this
    }
    private var handler: Handler? = null

    companion object {
        private var instance: App? = null


        fun applicationContext() : Context {
            return instance!!.applicationContext
        }

        fun getMainHandler() : Handler? {
            return instance!!.handler
        }

        fun getResource() = applicationContext().resources

    }

    override fun onCreate() {
        super.onCreate()

        val context: Context = App.applicationContext()
        handler = Handler(context.mainLooper)
    }
}