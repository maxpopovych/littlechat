package com.makki.kotlinstudies.views.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.makki.kotlinstudies.R
import com.makki.kotlinstudies.managers.ChatManager
import com.makki.kotlinstudies.views.rooms.RoomsActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_login.*

/**
 * @author Max.Popovych on 28.05.18.
 */
class LoginActivity : Activity(), View.OnClickListener, TextWatcher {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_button.setOnClickListener(this)
        login_name_view.addTextChangedListener(this)
    }

    override fun afterTextChanged(s: Editable?) {
        login_button.isEnabled = login_name_view.text.isNotBlank()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

    override fun onClick(v: View?) {
        val name = findViewById<EditText>(R.id.login_name_view).text.toString()
        ChatManager.register(name)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    spinner_view?.visibility = View.VISIBLE
                }
                .doOnComplete {
                    spinner_view?.visibility = View.GONE
                }
                .doAfterSuccess {
                    Toast.makeText(this, "You've successfully logged-in", Toast.LENGTH_SHORT).show()
                    startActivity(Intent().setClass(this, RoomsActivity::class.java))
                }
                .doOnError {
                    Toast.makeText(this, "Sorry, something went wrong", Toast.LENGTH_SHORT).show()
                }
                .subscribe()
    }
}
