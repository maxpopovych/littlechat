package com.makki.kotlinstudies.views.rooms

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.view.animation.AnimationUtils
import com.makki.kotlinstudies.ChatActivity
import com.makki.kotlinstudies.R
import com.makki.kotlinstudies.managers.ChatManager
import com.makki.kotlinstudies.model.RoomAsset
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.activity_rooms.*

class RoomsActivity : Activity(), SwipeRefreshLayout.OnRefreshListener, RoomsAdapter.RoomClickCallback {

    private var disposable = Disposables.disposed()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_rooms)
        room_list_view.animation = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom)


        disposable.dispose()
        disposable = ChatManager.getRoomList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    room_list_view.adapter = RoomsAdapter(it).also {
                        it.setClickListener(this)
                    }
                }

        refresh.setOnRefreshListener(this)
    }

    override fun onRefresh() {
        disposable.dispose()
        disposable = ChatManager.getRoomList()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete { refresh.isRefreshing = false }
                .subscribe {
                    (room_list_view.adapter as RoomsAdapter).setItems(it)
                    refresh.isRefreshing = false
                }
    }

    override fun onRoomClick(asset: RoomAsset) {
        disposable.dispose()
        disposable = ChatManager.joinRoom(asset.name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    startActivity(Intent().setClass(this, ChatActivity::class.java)
                            .also {
                        it.putExtra("extras", Bundle().apply { putParcelable("item", asset) })
                    })
                }
    }

}
