package com.makki.kotlinstudies.managers.socket

import android.util.Log
import com.makki.kotlinstudies.events.SocketRequest
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import java.util.concurrent.TimeUnit

/**
 * @author Max.Popovych on 13.05.18.
 */
class ChatSocket(client: OkHttpClient, request: Request, listener: WebSocketListener) {
    private var socket: WebSocket = client.newWebSocket(request, listener)

    public fun getSocket() = socket

    public fun sendRequest(request: SocketRequest?) {
        if (request == null) {
            throw IllegalArgumentException("Request is n")
        }
        Log.e("ChatSocket", "Trying to send message -> $request")
        socket.send(request.toString())
    }
}

class SocketBuilder {

    private var url: String = ""
    private var login: String = "Anon"
    private var timeOut: Long = 15000L
    private var listener: WebSocketListener? = null

    fun connectTo(url: String): SocketBuilder {
        this.url = url
        return this
    }

    fun timeOutIn(timeOut: Long): SocketBuilder {
        this.timeOut = timeOut
        return this
    }

    fun respondTo(listener: WebSocketListener): SocketBuilder {
        this.listener = listener
        return this
    }

    fun loginAs(login: String): SocketBuilder {
        this.login = login
        return this
    }

    fun build(): ChatSocket {
        if (listener == null) {
            throw Exception("Listener not set in SocketBuilder")
        }
        if (url.isEmpty()) {
            throw Exception("Url not set in SocketBuilder")
        }

        val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(timeOut, TimeUnit.MILLISECONDS)
                .build()

        val request: Request = Request.Builder()
                .addHeader("login", login)
                .url(url)
                .build()

        return ChatSocket(client, request, listener!!)
    }

}

