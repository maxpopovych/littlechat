package com.makki.kotlinstudies.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.makki.kotlinstudies.App
import com.makki.kotlinstudies.R

class MessageView : TextView {
    companion object {
        private val mBackPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        private val mBlackPaint = Paint(Paint.ANTI_ALIAS_FLAG)

        init {
            mBackPaint.color = App.applicationContext().resources.getColor(R.color.colorMessages)
            mBlackPaint.color = Color.BLACK
        }
    }

    private var setTailRight : Boolean = false

    private val pathCircle = Path()
    private val pathExclude = Path()

    private val padding : Int = 40
    private val endPadding : Int = padding + 40
    private val circleHeight : Int = 80

    init {
        textSize = 17F
        setTextColor(mBlackPaint.color)
    }

    constructor(context: Context, message: String) : super(context){
        text = message
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    public override fun onDraw(canvas: Canvas) {
        val end = paddingEnd / 2
        val endWidth = width - end

        val radius = (Math.min(height, endWidth) / 4).toFloat()
        if (setTailRight) {
            canvas.drawRoundRect(0f, 0f, endWidth.toFloat(), height.toFloat(), radius, radius, mBackPaint)
        } else {
            canvas.drawRoundRect(paddingLeft.toFloat() / 2, 0f, width.toFloat(), height.toFloat(), radius, radius, mBackPaint)
        }

        pathCircle.reset()
        if (setTailRight) {
            pathCircle.addArc((endWidth - end).toFloat(), radius, width.toFloat(), radius + circleHeight, 0f, 180f)
            pathExclude.reset()
            pathExclude.addArc((endWidth - end).toFloat(), radius + circleHeight / 3, width.toFloat(), radius + (circleHeight / 3) * 2, 0f, 180f)

        } else {
            pathCircle.addArc(0f, height - radius * 4, endPadding.toFloat(), height - radius, 0f, 180f)
            pathExclude.reset()
            pathExclude.addArc(0f, height - radius * 3, endPadding.toFloat(), height - radius * 2, 0f, 180f)
        }

        pathCircle.op(pathExclude, Path.Op.DIFFERENCE)
        canvas.drawPath(pathCircle, mBackPaint)

        super.onDraw(canvas)
    }

    override fun setLayoutParams(params: ViewGroup.LayoutParams?) {
        super.setLayoutParams(params)

        if (params != null && params is LinearLayout.LayoutParams) {
            setTailRight = params.gravity == Gravity.RIGHT

            if (setTailRight) {
                setPadding(padding, padding, endPadding, padding)
                textAlignment = View.TEXT_ALIGNMENT_VIEW_END
                params.topMargin = 5
            } else {
                setPadding(endPadding, padding, padding, padding)
                textAlignment = TEXT_ALIGNMENT_VIEW_START
            }
            params.topMargin = 5
        }
    }
}