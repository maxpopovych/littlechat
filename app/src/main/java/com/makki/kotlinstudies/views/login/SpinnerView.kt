package com.makki.kotlinstudies.views.login

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.makki.kotlinstudies.App
import com.makki.kotlinstudies.R

/**
 * @author Max.Popovych on 24.06.18.
 */
class SpinnerView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).also {
        it.color = App.getResource().getColor(R.color.colorMessages)
    }

    private val animator = ObjectAnimator.ofFloat(this, "rotation", 0f, 1f)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.save()

        canvas.drawArc(0f, 0f, canvas.width.toFloat(), canvas.height.toFloat(), 40f, 360f, true, paint)

        canvas.restore()
    }

    override fun setVisibility(visibility: Int) {
        if (visibility == View.VISIBLE && isAttachedToWindow) animator.start()
        else animator.pause()
    }

}