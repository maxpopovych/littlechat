package com.makki.kotlinstudies.model

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Max.Popovych on 01.06.18.
 */
class UserAsset(public val id: Int, public val name: String, public val online: Boolean) : Parcelable{
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeByte(if (online) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserAsset> {
        override fun createFromParcel(parcel: Parcel): UserAsset {
            return UserAsset(parcel)
        }

        override fun newArray(size: Int): Array<UserAsset?> {
            return arrayOfNulls(size)
        }
    }
}