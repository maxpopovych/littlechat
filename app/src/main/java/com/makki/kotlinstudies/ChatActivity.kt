@file:Suppress("DEPRECATION")

package com.makki.kotlinstudies

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.makki.kotlinstudies.R.id.inputView
import com.makki.kotlinstudies.managers.ChatManager
import com.makki.kotlinstudies.model.RoomAsset
import com.makki.kotlinstudies.views.MessageLayoutWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author Max.Popovych on 01.03.18.
 */
class ChatActivity : Activity(), TextView.OnEditorActionListener {

    private val layoutControls = MessageLayoutWrapper()
    private var roomAsset: RoomAsset? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        roomAsset = intent.getBundleExtra("extras").getParcelable("item")

        setContentView(R.layout.activity_main)
        inputView.setOnEditorActionListener(this)

        layoutControls.setTarget(messageLog)

        ChatManager.trackMessagesFrom(0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { msg ->
                    layoutControls.feed(msg)
                }

        ChatManager.trackBroadcasts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { resp ->
                    layoutControls.feed(resp)
                }

        messageLog.setOnHierarchyChangeListener(object: ViewGroup.OnHierarchyChangeListener {
            override fun onChildViewRemoved(parent: View?, child: View?) {
            }

            override fun onChildViewAdded(parent: View?, child: View?) {
                scrollView.fullScroll(View.FOCUS_DOWN)
            }

        })

    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE && !inputView.text.isNullOrBlank()) {
            sendTextMessage(inputView.text.toString(), roomAsset?.name ?: "")
            inputView.text.clear()
            return true
        }
        return false
    }

    private fun sendTextMessage(text: String, roomName: String) {
        ChatManager.sendTextMessage(text, roomName).subscribe()
    }

    private fun scrollToBottom() {

    }
}