package com.makki.kotlinstudies.model

import com.makki.kotlinstudies.events.SocketResponse
import com.makki.kotlinstudies.utils.getJsonLong
import com.makki.kotlinstudies.utils.getJsonString

/**
 * @author Max.Popovych on 13.05.18.
 */

class MessageAsset(response: SocketResponse) {
    var text: String = response.message
    var sender: String = response.content.getJsonString("sender", "Anon")
    var background: Boolean = response.command == "text_message"
    var timestamp: Long = response.content.getJsonLong("timestamp", System.currentTimeMillis())
}

