package com.makki.kotlinstudies.utils

import android.os.Handler
import org.json.JSONObject

/**
 * @author Max.Popovych on 19.04.18.
 */

inline fun runWithDelay(delay: Long, crossinline body: () -> Unit) {
    Handler().postDelayed({ body() }, delay)
}

fun String.getJsonLong(field: String) : Long {
    return this.getJsonLong(field, 0L)
}
fun String.getJsonLong(field: String, fallback: Long) : Long {
    return JSONObject(this).optLong(field, fallback)
}

fun String.getJsonString(field: String) : String {
    return this.getJsonString(field, "")
}
fun String.getJsonString(field: String, fallback: String) : String {
    return JSONObject(this).optString(field, fallback)
}