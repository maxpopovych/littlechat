package com.makki.kotlinstudies.managers.socket

import android.util.Log
import android.util.SparseArray
import android.widget.Toast
import com.makki.kotlinstudies.App
import com.makki.kotlinstudies.events.SResponseType
import com.makki.kotlinstudies.events.SocketRequest
import com.makki.kotlinstudies.events.SocketResponse
import io.reactivex.*
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.atomic.AtomicInteger

/**
 * @author Max.Popovych on 06.03.18.
 */

object SocketManager : Consumer<SocketResponse> {

    private const val TAG: String = "SocketManager"
    private const val LOCAL_HOST: String = "ws://10.0.2.2:8080/"
    private const val ECHO_HOST: String = "wss://echo.websocket.org"

    private var loginName: String = "Guest"

    private val events: Subject<SocketResponse> = BehaviorSubject.create()
    private val wrapper: SocketSubject = SocketSubject(events)
    private var connectionJob = initTask()
    private var socket: ChatSocket? = null

    private val counter = AtomicInteger(0)
    private val pendingMap = SparseArray<MaybeEmitter<SocketResponse>>()

    init {
        trackResponses().subscribe()
    }

    override fun accept(response: SocketResponse) {
        if (response.type == SResponseType.ClosedMessage) {
            closeConnection()
            clearPendingRequests()
        }

        if (response.type == SResponseType.FailureMessage) {
            onError()
            closeConnection()
            clearPendingRequests()
        }

        if (pendingMap[response.id] != null) {
            response.pending = true

            synchronized(this) {
                val emitter = pendingMap[response.id]
                pendingMap.remove(response.id)
                emitter.onSuccess(response)
            }
        }
    }

    public fun setLogin(login: String) {
        loginName = login
    }

    public fun getLogin() = loginName

    public fun sendRequest(msg: SocketRequest) =
            connectionJob.andThen(Completable.fromAction { socket?.sendRequest(msg) })
                    .subscribeOn(Schedulers.io())!!

    public fun sendPendingRequest(msg: SocketRequest) =
            connectionJob.andThen(Completable.fromAction { socket?.sendRequest(msg) })
                    .andThen(Maybe.create(SubscribeToResponse(this, msg)))
                    .subscribeOn(Schedulers.io())!!

    public fun trackResponses() = events.doOnNext(this)
            .subscribeOn(Schedulers.io())!!

    private fun initTask() = Completable
            .fromAction { reconnectSocket() }
            .cache()

    private fun reconnectSocket() {
        Log.w(TAG, "Trying to reconnect")

        socket = SocketBuilder()
                .loginAs(loginName)
                .connectTo(LOCAL_HOST)
                .respondTo(wrapper)
                .timeOutIn(5000)
                .build()
    }

    private fun closeConnection() {
        socket?.getSocket()?.cancel()
        connectionJob = initTask()
    }

    private fun onError() {
        App.getMainHandler()?.post {
            Toast.makeText(App.applicationContext(), "Sorry, something went wrong", Toast.LENGTH_SHORT).show()
        }
    }

    private fun clearPendingRequests() {
        for (i in 0..pendingMap.size()) {
            pendingMap[i]?.onComplete()
        }
        pendingMap?.clear()
    }

    fun getNextRequestId() = counter.incrementAndGet()

    fun registerPendingRequest(respondTo: MaybeEmitter<SocketResponse>, request: SocketRequest) {
        synchronized(this) {
            pendingMap.put(request.id, respondTo)
        }
    }
}

class SubscribeToResponse(private val manager: SocketManager, private val request: SocketRequest)
    : MaybeOnSubscribe<SocketResponse> {

    override fun subscribe(e: MaybeEmitter<SocketResponse>) {
        manager.registerPendingRequest(e, request)
    }

}